const expect = require('chai').expect;
const groupArrayElements = require('./index')

describe('groupArrayElements', () => {

  it('should return an array with 3 sub-arrays with length: 2,2,1', () => {
    expect(groupArrayElements([1, 2, 3, 4, 5], 3)).to.deep.equal([[1, 2], [3, 4], [5]])
  });

  it('should return an array with 5 sub-arrays with length 1', () => {
    expect(groupArrayElements([1, 2, 3, 4, 5], 5)).to.deep.equal([[1], [2], [3], [4], [5]])
  });

  it('should return an array with 5 sub-arrays with length 1 when N is bigger then the original array length', () => {
    expect(groupArrayElements([1, 2, 3, 4, 5], 9)).to.deep.equal([[1], [2], [3], [4], [5]])
  });

  it('should return an array with 3 sub-arrays with length 2', () => {
    expect(groupArrayElements([1, 2, 3, 4, 5, 6], 3)).to.deep.equal([[1, 2,], [3, 4], [5, 6]])
  });

  it('should return the original array if N is 0', () => {
    expect(groupArrayElements([1, 2, 3, 4, 5, 6], 0)).to.deep.equal([1, 2, 3, 4, 5, 6])
  });

});