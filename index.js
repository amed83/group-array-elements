
const groupArrayElements = (data, N) => {
  // this util is going to group the array elements into N sub-arrays
  // each of those with the same length where possible
  // this means that the last sub arrays could have a different length
  // you can run the test to see different use case.
  if (N < 1) {
    return data
  }
  // if N is > arr.length use the arr.length to calculate the length of subArrays
  const lengthOfSubArr = Math.round(data.length / (N <= data.length ? N : data.length));
  const numberOfSubArrays = N <= data.length ? N : data.length

  for (let i = 0; i < numberOfSubArrays; i++) {
    // when you reach the last element,slice till the end of the array
    const subArray = data.slice(i, i == N - 1 ? undefined : i + lengthOfSubArr)
    data.splice(i, subArray.length, subArray)
  }
  return data
}


module.exports = groupArrayElements;